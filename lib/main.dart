import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Location Demo',
      home: LocationScreen(),
    );
  }
}

class LocationScreen extends StatefulWidget {
  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  late Position lastPosition;
  late Position currentLocation;
  late double lat;
  late double long;
  late StreamSubscription<Position> _positionStreamSubscription;

  @override
  void initState() {
    super.initState();

    _determinePosition().then((value) {
      currentLocation = value;
      lat = currentLocation.latitude;
      long = currentLocation.longitude;
    }).onError((error, stackTrace) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("$error")));
    }).whenComplete(() {
      Geolocator.getLastKnownPosition().then((lastPosition) {
        if (lastPosition != null) {
          currentLocation = lastPosition;
        }
      }).whenComplete(() {
        subscibeLocation();
      });
    });
  }

  @override
  void dispose() {
    _positionStreamSubscription
        .cancel(); // Cancel the stream subscription when the widget is disposed
    super.dispose();
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error("GPS is not enabled");
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location Permission denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error("Location Permission permanently denied");
    }

    return await Geolocator.getCurrentPosition();
  }

  void subscibeLocation() {
    _positionStreamSubscription =
        Geolocator.getPositionStream().listen((Position? position) {
      if (position == null) {
        setState(() {
          lat = 0.0;
          long = 0.0;
        });
      } else {
        setState(() {
          currentLocation = position;
          lat = position.latitude;
          long = position.longitude;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Location Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Latitude: ${lat.toString()}'),
            Text('Longitude: ${long.toString()}'),
          ],
        ),
      ),
    );
  }
}
